package _05_Abstraction;

public abstract class Forme {
	
	public Forme() {
		System.out.println("Appel au constructeur de la classe abstraite");
	}

	/*
	 * Une m�thode abstraite devra OBLIGATOIREMENT �tre impl�ment�e dans les classes filles
	 */
	public abstract double surface();

	@Override
	public String toString() {
		return "Je suis une forme g�om�trique";
	}
}
