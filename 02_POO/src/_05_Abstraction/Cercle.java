package _05_Abstraction;

public class Cercle extends Forme{

	private int rayon;
	
	public int getRayon() {
		return rayon;
	}

	public void setRayon(int rayon) {
		
		if(rayon >= 0)
		{
			this.rayon = rayon;
		}
		else
		{
			throw new IllegalArgumentException("le rayon du cercle doit �tre positif");
		}
		
//		if(rayon < 0)
//		{
//			throw new IllegalArgumentException("le rayon du cercle doit �tre positif");
//		}
//		
//		this.rayon = rayon;
		
	}

	public Cercle(int rayon) {
		super();
		this.rayon = rayon;
	}

	@Override
	public double surface() {
		
		//return Math.PI * rayon * rayon;
		
		// ATTENTION : Math.round() retourne un entier. Donc il faut diviser par un double pour ne pas faire une division enti�re
		return Math.round(Math.PI * rayon * rayon * 100) / 100.0; 
	}
	
	@Override
	public String toString() {
		return super.toString() + " Je suis un cercle de rayon " + rayon;
	}
}
