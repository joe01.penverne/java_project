package _10_Exercice;

abstract class Calcul_Chiffre_Affaire extends Employe {
    private double chiffreAffaire;

    public Calcul_Chiffre_Affaire(String prenom, String nom, int age, String date,
               double chiffreAffaire) {
        super(prenom, nom, age, date);
        this.chiffreAffaire = chiffreAffaire;
    }

    public double getChiffreAffaire()
        {
            return chiffreAffaire;
        }

}
