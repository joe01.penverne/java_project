package _06_Methodes;

/*
 * Une m�thode reprsente un bloc d'instructions r�utilisable.
 * Elle permet d'viter les r�p�titions de code.
 * Une m�thode se diff�rencie d'une fonction en ce qu'elle est necessazirement ratach�e � un objet.
 * Etant purement objet, Java ne supporte que le concept de m�thode.
 * 
 * D�claration (signature) :
 * 
 * Visibilit� [mot-cl�] type-retour nomMethode(liste des param�tres){instructions}
 * 
 * Une m�thode peut avoir plusieurs signatures/prototypes (avec diff�rentes param�tres)
 * => On parle alors de "surcharge" ed m�thode
 */

public class Methodes {

	/**
	 * M�thode afficher
	 */
	public static void afficher() {
		System.out.println("M�thode afficher");
	}

	/**
	 * M�thode afficher surcharg�e
	 * 
	 * @param message
	 */
	public static void afficher(String message) {
		System.out.println(message);
	}

	/**
	 * 
	 * @param tab
	 */
	public static void afficher(int[] tab) {
		System.out.println("\nM�thode afficher tableau d'entiers");

		for (int i = 0; i < tab.length; i++) {
			System.out.println(tab[i]);
		}
	}

	/**
	 * Retourne la somme de 2 entiers.
	 * 
	 * @param a : premier �l�ment � sommer
	 * @param b : second �l�ment � sommer
	 * @return : la somme des 2 entiers pass�s en param�tres
	 */
	public static int somme(int a, int b) {
		return a + b;
	}

	/**
	 * Retourne la somme d'un tableau d'entiers pass� en param�tre
	 * 
	 * @param tab : tableau d'entiers
	 * @return : la somme du tableau d'entiers
	 */
	public static int somme(int[] tab) {
		int result = 0;

		for (int item : tab) {
			result += item;
		}

		return result;
	}

	/*
	 * Une m�thode r�cursive est une m�thode qui s'appuie sur sa propre ex�cution.
	 * Elles s'appelle donc elle-m�me pour calculer le r�sultat. Un algorithme
	 * r�cursif doit en premier lieu envisager les cas terminaux (pour arr�ter la
	 * r�cursivit�)
	 */

	/**
	 * Retourne la puissance d'un nombre
	 * @param value : nombre � �lever � une puissance
	 * @param pow : puissance
	 * @return : le nombre �lev� � la puissance
	 */
	public static int power(int value, int pow) {
		if (pow == 0) // cas terminal
			return 1;

		return value * power(value, pow - 1); 
	}

	public static void main(String[] args) {

		afficher();

		afficher("M�thode afficher surcharg�e avec un param�tre");

		int[] tab = { 10, 20, 30 };

		afficher(tab);

		afficher("Somme(2,3) = " + somme(2, 3));
		afficher("Somme({ 10, 20, 30 }) = " + somme(tab));
		
		afficher("power(2,3) = " + power(2,3));
	}
}
