package _03_Types._01_Types_primitifs;

public class _03_Flottants {
	
	public static void main(String[] args) {

		/*
		 * 2 types numériques flottants : 
		 * - float sur 4 octets (32 bits)
		 * - double sur 8 octets ( 64 bits)
		 */
		
		System.out.printf(" - %s (%d bits) from %e to %e\n", Float.TYPE, Float.SIZE, Float.MIN_VALUE, Float.MAX_VALUE);
		System.out.printf(" - %s (%d bits) from %e to %e\n", Double.TYPE, Double.SIZE, Double.MIN_VALUE, Double.MAX_VALUE);
		
		// Notation traditionnelle
		float myFloat = 3.4f;
		double myDouble = 3.4;
		
		myDouble = 1_000_000_000_000.99;
		
		System.out.println("myDouble = " + myDouble);
		
		// Notation exponentielle
		float myFloatExp = 3.4e10f;
		double myDoubleExp = 3.4e10;
		
		System.out.println("myFloatExp = " + myFloatExp);
		
		System.out.println(Double.POSITIVE_INFINITY);
		System.out.println(Double.NEGATIVE_INFINITY);
		System.out.println(Double.NaN); // Nan : Not a Number
		
		System.out.println(Float.isFinite(Float.NEGATIVE_INFINITY)); // false
		
		System.out.println(2.2/0.0); // Infinity
		System.out.println(0.0/0.0); // NaN
		
		if (Double.isNaN(0.0/0.0)) {
			System.out.println("Is NOT A Number");
		}
		else
		{
			System.out.println("Is A Number");
		}
	}
}
