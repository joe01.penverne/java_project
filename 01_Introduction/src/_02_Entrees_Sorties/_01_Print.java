package _02_Entrees_Sorties;

public class _01_Print {

	/*
	 * En cas de probl�me avec les caract�res accentu�s :
	 * - Window => Preferences => General => workspace => Text File Encoding : Default(Cp1252)
	 * - Menu Run => Run Configurations => Common => Default inherited cp1252
	 */
	
	public static void main(String[] args) {
		
		System.out.println("La m�thode \"println\" permet d'�crire une ligne");
		
		// Le caract�re "\" est un caract�re d'�chappement permettant d'�crire des caract�res sp�ciaux dans une chaine de caract�res
		
		System.out.print("La m\u00e9thode \"print\" permet ");
		System.out.print("d'�crire une chaine de carct�res ");
		System.out.print("sans retour � la ligne.");
		
		System.out.print("\nIl est n�anmoins possible de revenir � la ligne avec \\n.\n");
		
		String str = "printf";
		
		System.out.printf("La m�thode %s permet d'�crire des chaines de caract�res format�es", str);
		
		/*
		 * %s	Permet de formater une chaine de caract�res
		 * %c	Permet de formater un caract�re
		 * %d	Permet de formater un entier en base d�cimale
		 * %x	Permet de formater un entier sous forme hexadecimale
		 * %e	Permet de formater un r�el sous forme d�ciamele en notation scientifique
		 * %f	Permet de formater un r�el sousforme d�cimale
		 * %t	Permet de formater les dates
		 */
		
		int monEntier = 25;
		
		System.out.printf("\nMon entier vaut %d\n", monEntier);
		
		System.out.println("Mon entier vaut " + monEntier); // Plus simple avec concat�nation...
	
		System.out.printf("%f\n", Math.PI);
		System.out.printf("%.4f\n", Math.PI);
	}
}
