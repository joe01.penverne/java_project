package _07_Exercices;

public class Exo08_Tableau2 {

	/**
	 * Suppression d�un �l�ment dans un tableau tri�
	 * 
	 * La suppression d�un �l�ment d�un tableau qui contient une liste de donn�es d�cale vers la gauche les �l�ments situ�s � droite de l��l�ment � supprimer.
	 * Le premier d�calage �crase la valeur � supprimer par le contenu de la case de droite.
	 * Les d�calages successifs r�p�tent cette op�ration sur les cases suivantes, jusqu�� la fin des donn�es.
	 * 
	 * Cr�er et initialiser un tableau, puis supprimer un �l�ment de ce tableau � la position sp�cifi�e (de 0 � N-1).
	 * Pour supprimer un �l�ment du tableau, d�placez les �l�ments, juste apr�s la position donn�e vers une position � gauche et r�duisez la taille du tableau.
	 * Exemple :
	 * 
	 * Donn�es d'entr�e : Saisir le nombre de notes :
	 * 
	 * Note 1 : 8.5
	 * Note 2 : 9.5
	 * Note 3 : 11
	 * Note 3 : 12.5
	 * Note 4 : 18.0
	 * Saisir la position de l'�l�ment � supprimer : 2
	 * Donn�es de sortie : [8.5, 11.0, 12.5, 18.0]
	 * 
	 */
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
