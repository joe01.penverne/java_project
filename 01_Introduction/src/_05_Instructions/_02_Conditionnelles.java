package _05_Instructions;

import java.util.Scanner;

public class _02_Conditionnelles {

	public static void main(String[] args) {

		Scanner clavier = new Scanner(System.in);

		System.out.println("Entrez un nombe entier svp :");

		int n = clavier.nextInt();

		if (n > 0) {
			System.out.println("Le nombre est positif");
		} else if (n == 0) {
			System.out.println("Le nombre vaut 0");
		} else if (n * n == 25) {
			System.out.println("Le nombre vaut -5");
		} else {
			System.out.println("Le nombre est n�gatif");
		}

		System.out.println("Entrez le num�ro du jour de la semaine :");
		
		int jour = clavier.nextInt();
		
		switch (jour) {
		case 1:
			System.out.println("lundi");
			break;
		case 2:
			System.out.println("mardi");
			break;
		case 3:
			System.out.println("mercredi");
			break;
		case 4:
			System.out.println("jeudi");
			break;
		case 5:
			System.out.println("vendredi");
			break;
		case 6:
			System.out.println("samedi");
			break;
		case 7:
			System.out.println("dimanche");
			break;

		default:
			System.out.println("Entr�e invalide");
			break;
		}
		
		switch (jour) {
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			System.out.println("Jour de semaine");
			break;
		case 6:
		case 7:
			System.out.println("Weekend");
			break;

		default:
			System.out.println("Entr�e invalide");
			break;
		}
		
		clavier.close();
	}
}
